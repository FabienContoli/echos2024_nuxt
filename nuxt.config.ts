// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ['~/assets/scss/normalize.scss', '~/assets/scss/main.scss'],
  app: {
    pageTransition: { name: 'page', mode: 'out-in' },
    head: {
      title: 'Echos 2024',
      link: [
        { rel: 'icon', type: 'image/png', href: '/favicon-16x16.png' }
      ],
      htmlAttrs: {
        lang: 'fr',
      },
      meta :[
        {name:'description',content :"rendez vous le vendredi 31 Mai 2024 à partir de 16h pour l'édition 2024 du festival echos à la ferme du Faï"},
        {name:'keywords',content :"festival, echos, trompes, dome, fai, association, Dôme, ferme, musique, 2024"},
      ],
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
    }
  }
})
